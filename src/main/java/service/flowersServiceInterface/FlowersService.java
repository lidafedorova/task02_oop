package service.flowersServiceInterface;

import entity.Flowers;

import java.util.List;

public interface FlowersService {
    List<Flowers>deleteflower(List<Flowers>flowers,Flowers flower);
    List <Flowers> sortFlowers(List<Flowers>flowers);
}
