package service.flowersServiceInterface;

import entity.Bouquet;
import entity.Flowers;

import java.util.List;

public interface BouquetService{
    void showBouquet(List<Bouquet> bouquets);
    public Bouquet deleteflower(Bouquet bouquet,Flowers flower);
    public Bouquet addflower(Bouquet bouquet,Flowers flower);

}
