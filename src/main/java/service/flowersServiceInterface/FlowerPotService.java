package service.flowersServiceInterface;


import entity.FlowerPot;
import entity.Flowers;



public interface FlowerPotService {
     FlowerPot deleteFlowerFromPot(FlowerPot flowerPot, Flowers flower);
     FlowerPot addFlowerToPot(FlowerPot flowerPot, Flowers flower);
}
