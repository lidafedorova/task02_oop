package service.flowersService;
import entity.*;
import service.flowersServiceInterface.FlowersService;

import java.util.*;

public class FlowersServiceImp implements FlowersService {
    @Override
    public List<Flowers> deleteflower(List<Flowers> flowers, Flowers flower) {
        List<Flowers>flowersList=flowers;
        Iterator<Flowers>flowersIterator=flowersList.iterator();
            while (flowersIterator.hasNext()){
                    if (flowersIterator.next().equals(flower)){
                        flowersIterator.remove();
                    }
            }

        return flowersList;
    }
    FlowerPotServiceImp flowerPotServiceImp=new FlowerPotServiceImp();
    @Override
    public List<Flowers> sortFlowers(List<Flowers> flowers) {
        List<Flowers>list=flowers;
        Collections.sort(list);
        return list;

    }


}
