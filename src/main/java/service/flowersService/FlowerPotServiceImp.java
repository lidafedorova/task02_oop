package service.flowersService;
import entity.Bouquet;
import entity.FlowerPot;
import entity.Flowers;
import service.flowersServiceInterface.FlowerPotService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class FlowerPotServiceImp implements FlowerPotService {
    @Override
  public FlowerPot deleteFlowerFromPot(FlowerPot flowerPot, Flowers flower) {

        FlowerPot flowerPot1=flowerPot;
        List<Flowers>list=flowerPot.getFlowers();

        Iterator<Flowers> PotIterator=list.iterator();
      while (PotIterator.hasNext()){
          if (PotIterator.next().equals(flower)){
              PotIterator.remove();
           }
       }
      flowerPot1.setFlowers(list);
      return flowerPot1;
    }

    @Override
    public FlowerPot addFlowerToPot(FlowerPot flowerPot, Flowers flower) {
        FlowerPot pot=flowerPot;
        List<Flowers> addflow=pot.getFlowers();
       addflow.add(flower);
        for (Flowers flowers : addflow) {
            System.out.println(flowers);
        }
        pot.setFlowers(addflow);
        return pot;
    }

}
