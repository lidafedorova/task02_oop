package service.flowersService;

import entity.Bouquet;
import entity.Flowers;
import service.flowersServiceInterface.BouquetService;

import java.util.Iterator;
import java.util.List;

public class BouquetServiceImp implements BouquetService {
    @Override
    public void showBouquet(List<Bouquet> bouquets) {
        for (Bouquet b : bouquets) {
            System.out.println(b);
        }
    }

    @Override
    public Bouquet deleteflower(Bouquet bouquet, Flowers flower) {
        List<Flowers>list=bouquet.getFlowers();
        Bouquet b=bouquet;
        Iterator<Flowers>flowersIterator=list.iterator();
        while (flowersIterator.hasNext()){
            if (flowersIterator.next().equals(flower)){
                flowersIterator.remove();
            }
        }
        b.setFlowers(list);
        return b;
    }

    @Override
    public Bouquet addflower(Bouquet bouquet, Flowers flower) {
        List<Flowers>flowersList=bouquet.getFlowers();
        Bouquet bouquet1=bouquet;
        flowersList.add(flower);
        bouquet1.setFlowers(flowersList);
        return bouquet1;
    }


}
