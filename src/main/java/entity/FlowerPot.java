package entity;

import java.util.*;
public class FlowerPot implements Comparable<FlowerPot>{
    public FlowerPot( Double price, String name, String color) {

        this.price = price;
        this.name = name;
        this.color = color;
    }



    private List<Flowers>flowers;
    private Double price;
    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public FlowerPot() {
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    public FlowerPot(List<Flowers> flowers) {
        this.flowers = flowers;
    }

    public List<Flowers> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flowers> flowers) {
        this.flowers = flowers;
    }

    public Double getPrice() {

        Double flowerPotPrice=0.0;
        for (Flowers fl:getFlowers()){
            flowerPotPrice+=fl.getPrice();
        }
        return flowerPotPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlowerPot flowerPot = (FlowerPot) o;
        return Objects.equals(flowers, flowerPot.flowers) &&
                Objects.equals(price, flowerPot.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowers, price);
    }

    @Override
    public String toString() {
        return "FlowerPot{" +
                "flowers=" + flowers +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public int compareTo(FlowerPot o) {
        return 0;
    }
}