package entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Flowers implements Comparable <Flowers>{
    private int price;
    private String name;
    private String color;
    void Flowers(){
        List<Flowers> list= new ArrayList<>();

    }

    public Flowers( int price, String name, String color) {

        this.price = price;
        this.name = name;
        this.color = color;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int compareTo(Flowers obj) {
        return Float.compare(this.price, obj.price);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flowers flowers = (Flowers) o;
        return price == flowers.price &&
                Objects.equals(name, flowers.name) &&
                Objects.equals(color, flowers.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name, color);
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}