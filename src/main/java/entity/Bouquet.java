package entity;


import java.util.*;

public class Bouquet{
    private Double price;
    private List<Flowers>flowers;
    public Bouquet() {

    }


    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Flowers> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flowers> flowers) {
        this.flowers = flowers;
    }

    public Bouquet(List<Flowers>flowers){
        this.flowers=flowers;
    }

    public Double getPrice(){

        Double bouquetPrice=0.0;

        List<Flowers>flowers=getFlowers();

        for (Flowers f:flowers){
            bouquetPrice+=f.getPrice();
        }
        return bouquetPrice;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bouquet bouquet = (Bouquet) o;
        return Objects.equals(flowers, bouquet.flowers) &&
                Objects.equals(price, bouquet.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowers, price);
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "flowers=" + flowers +
                ", price=" + price +
                '}';
    }
}